﻿
namespace FenixPC
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txturl = new System.Windows.Forms.TextBox();
            this.btniniciar = new System.Windows.Forms.Button();
            this.cmbPuerto = new System.Windows.Forms.ComboBox();
            this.cmbProtocolo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblprotocolo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblmedio = new System.Windows.Forms.Label();
            this.cmbMedio = new System.Windows.Forms.ComboBox();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txturl
            // 
            this.txturl.Location = new System.Drawing.Point(231, 151);
            this.txturl.Name = "txturl";
            this.txturl.ReadOnly = true;
            this.txturl.Size = new System.Drawing.Size(338, 20);
            this.txturl.TabIndex = 9;
            this.txturl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txturl.Visible = false;
            // 
            // btniniciar
            // 
            this.btniniciar.Location = new System.Drawing.Point(332, 265);
            this.btniniciar.Name = "btniniciar";
            this.btniniciar.Size = new System.Drawing.Size(81, 23);
            this.btniniciar.TabIndex = 8;
            this.btniniciar.Text = "Iniciar";
            this.btniniciar.UseVisualStyleBackColor = true;
            this.btniniciar.Click += new System.EventHandler(this.btniniciar_Click);
            // 
            // cmbPuerto
            // 
            this.cmbPuerto.FormattingEnabled = true;
            this.cmbPuerto.Location = new System.Drawing.Point(332, 182);
            this.cmbPuerto.Name = "cmbPuerto";
            this.cmbPuerto.Size = new System.Drawing.Size(171, 21);
            this.cmbPuerto.TabIndex = 7;
            // 
            // cmbProtocolo
            // 
            this.cmbProtocolo.FormattingEnabled = true;
            this.cmbProtocolo.Items.AddRange(new object[] {
            "DLMS",
            "DLT"});
            this.cmbProtocolo.Location = new System.Drawing.Point(332, 238);
            this.cmbProtocolo.Name = "cmbProtocolo";
            this.cmbProtocolo.Size = new System.Drawing.Size(171, 21);
            this.cmbProtocolo.TabIndex = 10;
            this.cmbProtocolo.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Puerto:";
            // 
            // lblprotocolo
            // 
            this.lblprotocolo.AutoSize = true;
            this.lblprotocolo.Location = new System.Drawing.Point(228, 241);
            this.lblprotocolo.Name = "lblprotocolo";
            this.lblprotocolo.Size = new System.Drawing.Size(55, 13);
            this.lblprotocolo.TabIndex = 12;
            this.lblprotocolo.Text = "Protocolo:";
            this.lblprotocolo.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(367, 89);
            this.label3.TabIndex = 13;
            this.label3.Text = "Fenix PC";
            // 
            // lblmedio
            // 
            this.lblmedio.AutoSize = true;
            this.lblmedio.Location = new System.Drawing.Point(228, 213);
            this.lblmedio.Name = "lblmedio";
            this.lblmedio.Size = new System.Drawing.Size(39, 13);
            this.lblmedio.TabIndex = 14;
            this.lblmedio.Text = "Medio:";
            // 
            // cmbMedio
            // 
            this.cmbMedio.FormattingEnabled = true;
            this.cmbMedio.Items.AddRange(new object[] {
            "Xbee",
            "Sonda optica"});
            this.cmbMedio.Location = new System.Drawing.Point(332, 210);
            this.cmbMedio.Name = "cmbMedio";
            this.cmbMedio.Size = new System.Drawing.Size(171, 21);
            this.cmbMedio.TabIndex = 15;
            this.cmbMedio.SelectedIndexChanged += new System.EventHandler(this.cmbMedio_SelectedIndexChanged);
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.Enabled = false;
            this.btnDesconectar.Location = new System.Drawing.Point(424, 266);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(81, 23);
            this.btnDesconectar.TabIndex = 16;
            this.btnDesconectar.Text = "Desconectar";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDesconectar);
            this.Controls.Add(this.cmbMedio);
            this.Controls.Add(this.lblmedio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblprotocolo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbProtocolo);
            this.Controls.Add(this.txturl);
            this.Controls.Add(this.btniniciar);
            this.Controls.Add(this.cmbPuerto);
            this.Name = "MainForm";
            this.Text = "FenixPC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txturl;
        private System.Windows.Forms.Button btniniciar;
        private System.Windows.Forms.ComboBox cmbPuerto;
        private System.Windows.Forms.ComboBox cmbProtocolo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblprotocolo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblmedio;
        private System.Windows.Forms.ComboBox cmbMedio;
        private System.Windows.Forms.Button btnDesconectar;
    }
}