﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FenixPC
{
    public partial class MainForm : Form
    {

        SerialPort serialPort1 = new SerialPort();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                GetPort(cmbPuerto);
                DeleteProccess();
            }
            catch
            {

            }
        }
        private void MainForm_FormClosing(Object sender, FormClosingEventArgs e)
        {
            try
            {
                DeleteProccess();
            }
            catch (Exception ex)
            {

            }
        }

        private bool GetPort(ComboBox comboBox)
        {
            bool status = false;
            comboBox.Text = string.Empty;
            comboBox.DataSource = SerialPort.GetPortNames();
            if (!string.IsNullOrEmpty(comboBox.Text))
            {
                serialPort1.PortName = comboBox.Text.ToString();
                status = true;
            }
            return status;
        }

        private void DeleteProccess()
        {
            Process[] middleware = Process.GetProcessesByName("middleware");
            foreach (Process process1 in middleware)
            {
                process1.Kill();
            }
            Process[] fenix = Process.GetProcessesByName("fenix");
            foreach (Process process2 in fenix)
            {
                process2.Kill();
            }
            Process[] firefox = Process.GetProcessesByName("Firefox");
            foreach (Process process3 in firefox)
            {
                process3.Kill();
            }
        }

        private void btniniciar_Click(object sender, EventArgs e)
        {
            string ruta = Directory.GetCurrentDirectory();
            string paridad = "n";
            string nombreserial = "sm";
            string velocidadserial = "bm";
            string paridadserial = "pm";
            try
            {
                if (!GetPort(cmbPuerto))
                {
                    MessageBox.Show("Puerto COM no habilitado");
                    return;
                }

                if (cmbMedio.SelectedItem == null)
                {
                    MessageBox.Show("Debe escoger medio");
                    cmbMedio.Focus();
                    return;
                }
                //else if (!cmbMedio.SelectedItem.ToString().Equals("Xbee"))
                //{
                //    if (cmbProtocolo.SelectedItem == null)
                //    {
                //        MessageBox.Show("Debe escoger protocolo");
                //        cmbProtocolo.Focus();
                //        return;
                //    }

                //    if (cmbProtocolo.SelectedItem.ToString().Equals("DLT"))
                //    {
                //        paridad = "e";
                //    }
                //}

                string com = cmbPuerto.SelectedItem.ToString();
                string medio = cmbMedio.SelectedItem.ToString();
                string protocolo = string.Empty;
                string comandomiddleware = string.Empty;
                string argumentosmiddleware = string.Empty;
                string comandofenix = string.Empty;
                string comandoweb = string.Empty;
                string argumentoweb = string.Empty;
                string velocidad = string.Empty;

                btniniciar.Enabled = false;
                btnDesconectar.Enabled = true;
                cmbPuerto.Enabled = false;
                cmbMedio.Enabled = false;

                txturl.Text = @"https://127.0.0.1/menu/dispositivos?menu_activo=dispositivos";
                txturl.Visible = true;

                if (medio.Equals("Xbee"))
                {
                    velocidad = "115200";
                    nombreserial = "sx";
                    velocidadserial = "bx";
                    paridadserial = "px";
                }
                else
                {
                    velocidad = "9600";
                }

                comandomiddleware = @"middleware.exe";
                argumentosmiddleware = @"-"+nombreserial+" " + com + " -"+velocidadserial+" " + velocidad + " -"+paridadserial+" " + paridad + "";
                comandofenix = @"fenix.exe";
                comandoweb = @"firefox.exe";
                argumentoweb = @"https://127.0.0.1";


                if (cmbPuerto.Items.Count > 0)
                {
                    Console.WriteLine("CMD Executer V.0.1");
                    Console.WriteLine("Last Release 05 - Febraury - 2012");

                    ExecuteCommand(comandomiddleware, ruta, argumentosmiddleware);
                    ExecuteCommand(comandofenix, ruta);
                    Thread.Sleep(2500);
                    Process.Start(comandoweb, argumentoweb);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
          
        }

        static void ExecuteCommand(string _Command, string ruta = "", string argumentos = "")
        {
            //Para mas informacion consulte la ayuda de la consola con cmd.exe /? 

            ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + _Command);
            // Indicamos que la salida del proceso se redireccione en un Stream
            procStartInfo.RedirectStandardOutput = false;
            procStartInfo.UseShellExecute = true;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procStartInfo.CreateNoWindow = true;
            procStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            Process p = new Process();
            p.StartInfo = procStartInfo;
            p.StartInfo.WorkingDirectory = ruta;
            p.StartInfo.FileName = _Command;
            p.StartInfo.Arguments = argumentos;
            p.Start();
            //p.WaitForExit();
        }


        // Detecta USB o puerto serie virtual cuando lo conecta y desconecta del cable.
        protected override void WndProc(ref Message USB)
        {
            if (USB.Msg == 0x219)
            {
                cmbPuerto.DataSource = SerialPort.GetPortNames();
            }

            base.WndProc(ref USB); // Detecta si hay cambios en el usb y si los hay los refleja.
        }

        private void cmbMedio_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (cmbMedio.SelectedItem.ToString().Equals("Sonda optica"))
            //    {
            //        lblprotocolo.Visible = true;
            //        cmbProtocolo.Visible = true;
            //    }
            //    else
            //    {
            //        lblprotocolo.Visible = false;
            //        cmbProtocolo.Visible = false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message.ToString());
            //}
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteProccess();
                btnDesconectar.Enabled = false;
                btniniciar.Enabled = true;
                cmbPuerto.Enabled = true;
                cmbMedio.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
